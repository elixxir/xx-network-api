/* eslint-disable no-await-in-loop */
require('dotenv').config();
const camelcaseKeys = require('camelcase-keys');
const fs = require('fs');
const yargs = require('yargs');
const { knex: localDb } = require('../database/local');
const { knex: remoteDb } = require('../database/remote');

const fetchRemotely = async (rawSql, args) => {
  const { rows } = await remoteDb.raw(rawSql, args);
  return rows.map(camelcaseKeys);
};

const fetchLocally = async (rawSql, args) => {
  const { rows } = await localDb.raw(rawSql, args);
  return rows.map(camelcaseKeys);
};

const nodePerformancesQuery = fs.readFileSync('./database/selects/performance_metrics.sql').toString();
const intervalLength = 60 * 60 * 1000;

const formatDblinkQuery = (rawSql, args) => localDb.raw(rawSql, args)
  .toString()
  .replace(/'/g, '\'\'');

(async () => {
  const [{ t: lastScanned }] = await fetchLocally(`
    SELECT MAX(interval_end) as t
    FROM node_performance_metrics
  `);

  const [{ t: earliestNodeMetric }] = await fetchRemotely(`
    SELECT DATE_TRUNC('hour', MIN(end_time)) as t
    FROM node_metrics
  `);

  const { argv } = yargs.option('rescan', {
    description: 'Rescan node performance metrics from the beginning',
    type: 'boolean',
  });

  if (argv.rescan) {
    console.log('Rescanning node metrics');
  }

  let intervalStart = (!argv.rescan && lastScanned)
    ? new Date(lastScanned.getTime() - intervalLength) // update the last interval
    : new Date(earliestNodeMetric);

  while (intervalStart.getTime() < new Date().getTime()) {
    try {
      const intervalEnd = new Date(intervalStart.getTime() + intervalLength);
      const args = [
        intervalStart,
        intervalEnd,
        intervalStart,
        intervalEnd,
        intervalStart,
        intervalEnd,
        intervalStart,
        intervalEnd,
        intervalStart,
        intervalEnd,
        intervalStart,
        intervalEnd,
      ].map((t) => t.toISOString());
      const formattedPerformanceQuery = formatDblinkQuery(nodePerformancesQuery, args);

      await fetchLocally(`
        INSERT INTO node_performance_metrics (
          node_id,
          rounds_failed,
          realtime_rounds_failed,
          precomp_rounds_failed,
          rounds_participated,
          uptime_metric_count,
          node_metric_count,
          realtime_avg_seconds,
          precomp_avg_seconds,
          interval_start,
          interval_end
        ) SELECT * FROM dblink(
          '${process.env.REMOTE_DB_URL}',
          '${formattedPerformanceQuery}'
        ) as t(
          node_id text,
          rounds_failed integer,
          realtime_rounds_failed integer,
          precomp_rounds_failed integer,
          rounds_partipated integer,
          uptime_metric_count integer,
          node_metric_count integer,
          realtime_avg_seconds numeric,
          precomp_avg_seconds numeric,
          interval_start timestamptz,
          interval_end timestamptz
        ) ON CONFLICT (node_id, interval_start)
          DO UPDATE SET
            rounds_failed = excluded.rounds_failed,
            precomp_rounds_failed = excluded.precomp_rounds_failed,
            realtime_rounds_failed = excluded.realtime_rounds_failed,
            rounds_participated = excluded.rounds_participated,
            uptime_metric_count = excluded.uptime_metric_count,
            node_metric_count = excluded.node_metric_count,
            realtime_avg_seconds = excluded.realtime_avg_seconds,
            precomp_avg_seconds = excluded.precomp_avg_seconds;
      `);
      const intervalString = `${intervalStart.toISOString()}-${intervalEnd.toISOString()}`;
      console.log(`Inserted node metrics: ${intervalString}.`);
      intervalStart = intervalEnd;
    } catch (err) {
      console.error('Error while populating node_performance_metrics:', err.message);
    }
  }
  process.exit();
})().catch((err) => {
  console.error(err.message);
  process.exit(1);
});
