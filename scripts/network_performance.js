/* eslint-disable no-await-in-loop */
require('dotenv').config();
const camelcaseKeys = require('camelcase-keys');
const fs = require('fs');
const yargs = require('yargs');
const { knex: localDb } = require('../database/local');
const { knex: remoteDb } = require('../database/remote');

const fetchRemotely = async (rawSql, args) => {
  const { rows } = await remoteDb.raw(rawSql, args);
  return rows.map(camelcaseKeys);
};

const fetchLocally = async (rawSql, args) => {
  const { rows } = await localDb.raw(rawSql, args);
  return rows.map(camelcaseKeys);
};

const networkPerformanceQuery = fs.readFileSync('./database/selects/hourly_network_metrics.sql').toString();
const intervalLength = 60 * 60 * 1000;

const formatDblinkQuery = (rawSql, args) => localDb.raw(rawSql, args)
  .toString()
  .replace(/'/g, '\'\'');

(async () => {
  const [{ t: lastScanned }] = await fetchLocally(`
    SELECT MAX(interval_end) as t
    FROM network_performance_metrics
  `);

  const [{ t: earliestRoundMetric }] = await fetchRemotely(`
    SELECT DATE_TRUNC('hour', MIN(realtime_end)) as t
    FROM round_metrics
  `);

  const { argv } = yargs.option('rescan', {
    description: 'Rescan network performance metrics from the beginning',
    type: 'boolean',
  });

  if (argv.rescan) {
    console.log('Rescanning network metrics');
  }

  let intervalStart = (!argv.rescan && lastScanned)
    ? new Date(lastScanned.getTime() - intervalLength)
    : new Date(earliestRoundMetric);

  while (intervalStart.getTime() < new Date().getTime()) {
    try {
      const intervalEnd = new Date(intervalStart.getTime() + intervalLength);
      const args = [
        intervalStart,
        intervalEnd,
        intervalStart,
        intervalEnd,
        intervalStart,
        intervalEnd,
        intervalStart,
        intervalEnd,
      ].map((t) => t.toISOString());
      const formattedQuery = formatDblinkQuery(networkPerformanceQuery, args);
      const sql = `
      INSERT INTO network_performance_metrics (
        batch_size_sum,
        successful_rounds,
        precomp_rounds_failed,
        realtime_rounds_failed,
        realtime_avg_seconds,
        precomp_avg_seconds,
        interval_start,
        interval_end
      ) SELECT * FROM dblink(
        '${process.env.REMOTE_DB_URL}',
        '${formattedQuery}'
      ) as t(
        batch_size_sum bigint,
        successful_rounds integer,
        precomp_rounds_failed integer,
        realtime_rounds_failed integer,
        realtime_avg_seconds numeric,
        precomp_avg_seconds numeric,
        interval_start timestamptz,
        interval_end timestamptz
      ) ON CONFLICT (interval_start)
        DO UPDATE SET
          batch_size_sum = excluded.batch_size_sum,
          interval_end = excluded.interval_end,
          precomp_rounds_failed = excluded.precomp_rounds_failed,
          realtime_rounds_failed = excluded.realtime_rounds_failed,
          successful_rounds = excluded.successful_rounds,
          realtime_avg_seconds = excluded.realtime_avg_seconds,
          precomp_avg_seconds = excluded.precomp_avg_seconds;
      `;

      await fetchLocally(sql);

      console.log(`Inserted network performance metrics: ${intervalStart.toISOString()} - ${intervalEnd.toISOString()}`);
      intervalStart = intervalEnd;
    } catch (err) {
      console.error('Error while populating network_performance_metrics.');
    }
  }

  process.exit();
})().catch((err) => {
  console.error(err.message);
  process.exit(1);
});
