require('dotenv').config();
const { chunkPromise } = require('chunk-promise');
const whois = require('whois');
const { knex: remote } = require('../database/remote');
const { knex: local } = require('../database/local');

const scan = (ip) => new Promise((resolve, reject) => {
  whois.lookup(ip, { server: 'whois.pwhois.org' }, (err, data) => {
    if (err) { reject(err); } else { resolve(data); }
  });
});

(async () => {
  const nodes = await remote.select(['id', 'serverAddress']).from('nodes')
    .whereNotNull('serverAddress');

  const promises = nodes.map((node) => async () => {
    const ip = node.serverAddress.split(':')[0];
    if (/^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$/.test(ip)) {
      const whoisInfo = await scan(ip);
      const orgNameRegex = /AS-Org-Name: (.+)|Org-Name:(.+)/gm;
      const matched = orgNameRegex.exec(whoisInfo);
      const ispInfo = matched[1] || matched[2];
      if (ispInfo) {
        await local.raw(`
          INSERT INTO node_whois (node_id, whois, created_at, updated_at)
          VALUES (?, ?, ?, ?)
          ON CONFLICT (node_id)
          DO UPDATE SET
            whois = excluded.whois,
            updated_at = excluded.updated_at
        `, [
          node.id.toString('base64'),
          ispInfo,
          new Date().toUTCString(),
          new Date().toUTCString(),
        ]);
      } else {
        console.error('Error: ISP info not found for', ip);
      }
    }
  });
  await chunkPromise(promises, { concurrent: 10, sleepMs: 500 });
  process.exit();
})().catch((err) => {
  console.error(err.message);
  process.exit(1);
});
