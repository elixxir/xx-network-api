const { CronJob } = require('cron');
const EventEmitter = require('events');
const { fork } = require('promisify-child-process');
const queryable = require('queryable-promise');
const { knex } = require('../database/local');

const emitter = new EventEmitter();

const nodeStatuses = new CronJob(
  '* * * * *', // every minute
  async () => {
    await knex.raw('REFRESH MATERIALIZED VIEW CONCURRENTLY node_statuses');
    emitter.emit('node_statuses_updated');
  },
);

let nodeMetricsTask;
const performanceMetrics = new CronJob(
  '* * * * *', // every minute
  async () => {
    if (nodeMetricsTask && nodeMetricsTask.isPending()) {
      console.log('Performance metrics population already in progress. Skipping...');
      return;
    }
    console.time('Performance metrics refreshed');
    nodeMetricsTask = queryable(fork(`${__dirname}/node_performance.js`));
    await nodeMetricsTask;
    await knex.raw('REFRESH MATERIALIZED VIEW CONCURRENTLY monthly_node_performance_metrics');
    await knex.raw('REFRESH MATERIALIZED VIEW CONCURRENTLY weekly_node_performance_metrics');
    emitter.emit('node_failure_rates_updated');
    emitter.emit('node_uptimes_updated');
    emitter.emit('node_round_averages_updated');
    console.timeEnd('Performance metrics refreshed');
  },
);

let networkMetricsTask;
setInterval(async () => {
  if (networkMetricsTask && networkMetricsTask.isPending()) {
    console.log('Network metrics population already in progress. Skipping...');
    return;
  }
  console.time('Network metrics refreshed');
  networkMetricsTask = queryable(fork(`${__dirname}/network_performance.js`));
  await networkMetricsTask;
  console.timeEnd('Network metrics refreshed');
  emitter.emit('network_metrics_updated');
}, 30 * 1000);

const scanWhoIs = new CronJob(
  '*/15 * * * *',
  async () => {
    await queryable(fork(`${__dirname}/scan_whois.js`));
    emitter.emit('node_whois_info_updated');
  },
);

nodeStatuses.start();
performanceMetrics.start();
scanWhoIs.start();

module.exports = {
  eventBus: emitter,
};
