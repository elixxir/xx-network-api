require('dotenv').config();

const { knexSnakeCaseMappers } = require('objection');

const user = process.env.REMOTE_DB_USER;
const pass = encodeURIComponent(process.env.REMOTE_DB_PASS);
const db = process.env.REMOTE_DB_NAME;
const port = process.env.REMOTE_DB_PORT || '5432';
const host = process.env.REMOTE_DB_HOST || 'localhost';
const connection = process.env.REMOTE_DB_URL || `postgres://${user}:${pass}@${host}:${port}/${db}`;

module.exports = {
  client: 'pg',
  connection,
  ...knexSnakeCaseMappers(),
  pool: { min: 0, max: 20 },
};
