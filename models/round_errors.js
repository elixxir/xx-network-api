const { Model } = require('../database/remote');

class NodeMetrics extends Model {
  static get tableName() {
    return 'round_errors';
  }

  static get idColumn() {
    return 'id';
  }

  static get relationMappings() {
    return {
      node: {
        relation: Model.BelongsToOneRelation,
        modelClass: `${__dirname}/round_metrics`,
        join: {
          from: 'round_metrics.id',
          to: 'round_errors.roundMetricId',
        },
      },
    };
  }
}

module.exports = NodeMetrics;
