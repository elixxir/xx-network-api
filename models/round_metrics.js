const { Model, knex } = require('../database/remote');

const tableName = 'round_metrics';

class RoundMetrics extends Model {
  static get modifiers() {
    const { ref } = RoundMetrics;
    return {
      lastTwoDays(query) {
        return query.where(ref('realtimeEnd'), '>', knex.raw('now() - INTERVAL \'2 DAYS\''));
      },
      successful(query) {
        return query.whereNotExists(RoundMetrics.relatedQuery('errors'));
      },
    };
  }

  static get virtualAttributes() {
    return ['realtimeDuration', 'precompDuration', 'status'];
  }

  static get tableName() {
    return tableName;
  }

  static get idColumn() {
    return 'id';
  }

  get realtimeDuration() {
    return this.errors && this.errors.length > 0
      ? null
      : (new Date(this.realtimeEnd).getTime() - new Date(this.realtimeStart).getTime()) / 1000;
  }

  get precompDuration() {
    return this.errors && this.errors.length > 0
      ? null
      : (new Date(this.precompEnd).getTime() - new Date(this.precompStart).getTime()) / 1000;
  }

  $formatJson(json) {
    const formatted = super.$formatJson(json);

    // Epoch timestamps make it in the database for whatever reason so here's a
    // monkey patch for it
    if (formatted.realtimeStart && formatted.realtimeStart.getTime() === 0) {
      formatted.realtimeStart = null;
    }

    if (formatted.precompEnd && formatted.precompEnd.getTime() === 0) {
      formatted.precompEnd = null;
    }

    return formatted;
  }

  get status() {
    let status;
    if (this.errors) {
      if (this.errors.length === 0) {
        status = 'success';
      } else if (this.errors.length > 0) {
        status = 'timeout';
      }
    }
    return status;
  }

  static get relationMappings() {
    return {
      errors: {
        relation: Model.HasManyRelation,
        modelClass: `${__dirname}/round_errors`,
        join: {
          from: `${tableName}.id`,
          to: 'round_errors.roundMetricId',
        },
      },
      topologies: {
        relation: Model.HasManyRelation,
        modelClass: `${__dirname}/topology`,
        join: {
          to: 'round_metrics.id',
          from: 'topologies.roundMetricId',
        },
      },
      nodes: {
        relation: Model.ManyToManyRelation,
        modelClass: `${__dirname}/node`,
        join: {
          from: `${tableName}.id`,
          through: {
            from: 'topologies.round_metric_id',
            to: 'topologies.node_id',
            extra: ['order'],
          },
          to: 'nodes.id',
        },
      },
    };
  }
}

module.exports = RoundMetrics;
