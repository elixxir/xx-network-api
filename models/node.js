const base64url = require('base64url');
const { Model } = require('../database/remote');

class Node extends Model {
  static get virtualAttributes() {
    return ['base64Url'];
  }

  static get modifiers() {
    const { ref } = Node;
    return {
      defaultSelects(query) {
        query.select(
          ref('id'),
          ref('applicationId'),
          ref('dateRegistered'),
        );
      },
      filterActive(query) {
        return query.where('status', 1);
      },
    };
  }

  static get tableName() {
    return 'nodes';
  }

  static get idColumn() {
    return 'id';
  }

  static get hidden() {
    return ['code', 'nodeCertificate', 'gatewayCertificate', 'serverAddress'];
  }

  get base64Url() {
    return base64url(this.id);
  }

  $formatJson(json) {
    // Remember to call the super class's implementation.
    const formatted = super.$formatJson(json);
    // Do your conversion here.
    if (formatted.id) {
      formatted.id = formatted.id.toString('base64');
    }

    return formatted;
  }

  static get relationMappings() {
    return {
      rounds: {
        relation: Model.ManyToManyRelation,
        modelClass: `${__dirname}/round_metrics`,
        join: {
          from: 'nodes.id',
          through: {
            from: 'topologies.node_id',
            to: 'topologies.round_metric_id',
            extra: ['order'],
          },
          to: 'round_metrics.id',
        },
      },
      metrics: {
        relation: Model.HasManyRelation,
        modelClass: `${__dirname}/node_metrics`,
        join: {
          from: 'nodes.id',
          to: 'node_metrics.node_id',
        },
      },
      application: {
        relation: Model.BelongsToOneRelation,
        modelClass: `${__dirname}/application`,
        join: {
          from: 'nodes.applicationId',
          to: 'applications.id',
        },
      },
    };
  }
}

module.exports = Node;
