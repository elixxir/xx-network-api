const { Model } = require('../database/remote');

class Application extends Model {
  static get modifiers() {
    const { ref } = Application;
    return {
      locationSelects(query) {
        query.select(
          ref('gpsLocation'),
        );
      },
    };
  }

  static get tableName() {
    return 'applications';
  }

  static get idColumn() {
    return 'id';
  }

  static get relationMappings() {
    return {
      node: {
        relation: Model.HasOneRelation,
        modelClass: `${__dirname}/node`,
        join: {
          from: 'nodes.applicationId',
          to: 'applications.id',
        },
      },
    };
  }
}

module.exports = Application;
