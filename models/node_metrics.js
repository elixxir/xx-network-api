const { Model, knex } = require('../database/remote');

class NodeMetrics extends Model {
  static get modifiers() {
    const { ref } = NodeMetrics;
    return {
      lastTwoDays(query) {
        query.where(ref('endTime'), '>', knex.raw('now() - INTERVAL \'2 DAYS\''));
      },
    };
  }

  static get tableName() {
    return 'node_metrics';
  }

  static get idColumn() {
    return 'id';
  }

  $formatJson(json) {
    const formatted = super.$formatJson(json);

    if (formatted.nodeId) {
      formatted.nodeId = formatted.id.toString('base64');
    }

    return formatted;
  }


  static get relationMappings() {
    return {
      node: {
        relation: Model.BelongsToOneRelation,
        modelClass: `${__dirname}/node`,
        join: {
          from: 'node_metrics.nodeId',
          to: 'nodes.id',
        },
      },
    };
  }
}

module.exports = NodeMetrics;
