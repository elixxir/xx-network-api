const { Model } = require('../database/remote');

class Topology extends Model {
  static get tableName() {
    return 'topologies';
  }

  static get idColumn() {
    return ['node_id', 'round_metric_id'];
  }

  $formatJson(json) {
    // Remember to call the super class's implementation.
    const formatted = super.$formatJson(json);
    // Do your conversion here.
    formatted.nodeId = formatted.nodeId.toString('base64');
    return formatted;
  }

  static get relationMappings() {
    return {
      node: {
        relation: Model.BelongsToOneRelation,
        modelClass: `${__dirname}/node`,
        join: {
          from: 'topologies.nodeId',
          to: 'nodes.id',
        },
      },
      roundMetric: {
        relation: Model.BelongsToOneRelation,
        modelClass: `${__dirname}/round_metrics`,
        join: {
          from: 'topologies.roundMetricId',
          to: 'round_metrics.id',
        },
      },
    };
  }
}

module.exports = Topology;
