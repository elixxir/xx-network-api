module.exports = {
  shuffle(items) {
    let currentIndex = items.length;
    let temporaryValue;
    let randomIndex;
    const arr = items.slice();

    // While there remain elements to shuffle...
    while (currentIndex !== 0) {
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;

      // And swap it with the current element.
      temporaryValue = arr[currentIndex];
      arr[currentIndex] = arr[randomIndex];
      arr[randomIndex] = temporaryValue;
    }

    return arr;
  },
  average(items = []) {
    return items.length && items.reduce((a, b) => a + b) / items.length;
  },
  sum(items = []) {
    return items.reduce((a, b) => a + b, 0);
  },
  sortByDate(items, prop = 'date', asc = false) {
    const sorted = items
      .slice() // copy
      .sort((a, b) => new Date(b[prop]).getTime() - new Date(a[prop]).getTime());
    if (asc) {
      sorted.reverse();
    }
    return sorted;
  },
};
