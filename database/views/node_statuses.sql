SELECT
  encode(id, 'base64') AS node_id,
  CASE
    WHEN status = 0 THEN 'unregistered'
    WHEN status = 3 THEN 'banned'
    WHEN status = 255 THEN 'defunct'
    WHEN status = 1 AND (latest_num_pings = 0 OR latest_num_pings IS NULL) THEN 'offline'
    WHEN status = 1
      AND latest_num_pings > 0
      AND EXISTS
        (
          SELECT *
          FROM topologies t
          JOIN round_metrics rm
          ON rm.id = t.round_metric_id
          WHERE t.node_id = x.id
          AND rm.realtime_end > NOW() - INTERVAL '5 minutes'
        )
    THEN 'online'
  ELSE
    'error'
  END as status
FROM (
  SELECT
    n.id,
    n.status,
    n.application_id,
    CAST(num_pings as integer) as latest_num_pings
  FROM nodes n
  LEFT JOIN node_metrics nm
  ON n.id = nm.node_id
  WHERE n.id IS NOT NULL AND (nm.id IN (
    SELECT MAX(id) FROM node_metrics GROUP BY node_id
  ) OR nm.id IS NULL)
) x
