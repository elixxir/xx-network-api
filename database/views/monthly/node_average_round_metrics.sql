SELECT
  encode(node_id, 'base64') as node_id,
  realtime_avg_seconds,
  precomp_avg_seconds,
  month
FROM (
  SELECT
    node_id,
    extract('epoch' from AVG(r.realtime_end - r.realtime_start)) AS realtime_avg_seconds,
    extract('epoch' from AVG(r.precomp_end - r.precomp_start)) AS precomp_avg_seconds,
    date_trunc('month', r.realtime_end) AS month
  FROM topologies t
    RIGHT JOIN round_metrics r
    ON r.id = t.round_metric_id
  WHERE NOT EXISTS (SELECT round_metric_id from round_errors re WHERE re.round_metric_id = r.id)
  AND r.realtime_start != '1970-01-01'::date
  AND r.realtime_end != '1970-01-01'::date
  AND r.precomp_start != '1970-01-01'::date
  AND r.precomp_end != '1970-01-01'::date
  GROUP BY node_id, month
) t
