WITH cte_uptime (node_id, total_metric_count, uptime_metric_count) AS (
  SELECT
  DISTINCT nm.node_id,
  COUNT(*),
  SUM(CASE WHEN num_pings > 0 THEN 1 ELSE 0 END),
  DATE_TRUNC('week', nm.end_time) as week
  FROM node_metrics nm
  GROUP BY nm.node_id, week
)
SELECT
  encode(node_id, 'base64')::text as node_id,
  week,
  ROUND(
    ((t.uptime_metric_count / (SELECT MAX(total_metric_count) FROM cte_uptime WHERE week = t.week)::numeric) * 100)::numeric,
    2
  ) as uptime
FROM cte_uptime t
ORDER by node_id, week
