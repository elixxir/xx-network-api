WITH weekly_failed_rounds AS (
  SELECT
    t.node_id,
    COUNT(DISTINCT re.round_metric_id),
    DATE_TRUNC('week', rm.realtime_end) as week
  FROM topologies t
  RIGHT JOIN round_metrics rm
  ON rm.id = t.round_metric_id
  RIGHT JOIN round_errors re
  ON t.round_metric_id = re.round_metric_id
  GROUP BY t.node_id, week
), weekly_participated_rounds AS (
  SELECT
    node_id,
    COUNT(DISTINCT round_metric_id),
    DATE_TRUNC('week', rm.realtime_end) as week
  FROM topologies t
  JOIN round_metrics rm
  ON rm.id = t.round_metric_id
  GROUP BY node_id, week
)
SELECT
  encode(mfr.node_id, 'base64') as node_id,
  mfr.week,
  ROUND((100 * mfr.count::numeric / mpr.count::numeric), 2) AS round_failure_avg
FROM weekly_failed_rounds mfr
JOIN weekly_participated_rounds mpr ON mpr.node_id = mfr.node_id AND mpr.week = mfr.week
