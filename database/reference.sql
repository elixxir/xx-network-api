--
-- PostgreSQL database dump of the remote DB
--

-- Dumped from database version 11.3
-- Dumped by pg_dump version 11.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: public; Type: SCHEMA; Schema: -; Owner: jacobtaylor
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO jacobtaylor;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: jacobtaylor
--

COMMENT ON SCHEMA public IS 'standard public schema';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: applications; Type: TABLE; Schema: public; Owner: cmix
--

CREATE TABLE public.applications (
    id bigint NOT NULL,
    name text,
    url text,
    blurb text,
    location text,
    geo_bin text,
    gps_location text,
    team text,
    network text,
    email text,
    twitter text,
    discord text,
    instagram text,
    medium text
);


ALTER TABLE public.applications OWNER TO cmix;

--
-- Name: applications_id_seq; Type: SEQUENCE; Schema: public; Owner: cmix
--

CREATE SEQUENCE public.applications_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.applications_id_seq OWNER TO cmix;

--
-- Name: applications_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cmix
--

ALTER SEQUENCE public.applications_id_seq OWNED BY public.applications.id;


--
-- Name: node_metrics; Type: TABLE; Schema: public; Owner: cmix
--

CREATE TABLE public.node_metrics (
    id bigint NOT NULL,
    node_id bytea NOT NULL,
    start_time timestamp with time zone NOT NULL,
    end_time timestamp with time zone NOT NULL,
    num_pings bigint NOT NULL
);


ALTER TABLE public.node_metrics OWNER TO cmix;

--
-- Name: node_metrics_id_seq; Type: SEQUENCE; Schema: public; Owner: cmix
--

CREATE SEQUENCE public.node_metrics_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.node_metrics_id_seq OWNER TO cmix;

--
-- Name: node_metrics_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cmix
--

ALTER SEQUENCE public.node_metrics_id_seq OWNED BY public.node_metrics.id;


--
-- Name: nodes; Type: TABLE; Schema: public; Owner: cmix
--

CREATE TABLE public.nodes (
    code text NOT NULL,
    sequence text,
    id bytea,
    server_address text,
    gateway_address text,
    node_certificate text,
    gateway_certificate text,
    date_registered timestamp with time zone,
    status integer NOT NULL,
    application_id bigint NOT NULL
);


ALTER TABLE public.nodes OWNER TO cmix;

--
-- Name: registration_codes; Type: TABLE; Schema: public; Owner: cmix
--

CREATE TABLE public.registration_codes (
    code text NOT NULL,
    remaining_uses integer
);


ALTER TABLE public.registration_codes OWNER TO cmix;

--
-- Name: round_errors; Type: TABLE; Schema: public; Owner: cmix
--

CREATE TABLE public.round_errors (
    id bigint NOT NULL,
    round_metric_id bigint NOT NULL,
    error text NOT NULL
);


ALTER TABLE public.round_errors OWNER TO cmix;

--
-- Name: round_errors_id_seq; Type: SEQUENCE; Schema: public; Owner: cmix
--

CREATE SEQUENCE public.round_errors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.round_errors_id_seq OWNER TO cmix;

--
-- Name: round_errors_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cmix
--

ALTER SEQUENCE public.round_errors_id_seq OWNED BY public.round_errors.id;


--
-- Name: round_metrics; Type: TABLE; Schema: public; Owner: cmix
--

CREATE TABLE public.round_metrics (
    id bigint NOT NULL,
    precomp_start timestamp with time zone NOT NULL,
    precomp_end timestamp with time zone NOT NULL,
    realtime_start timestamp with time zone NOT NULL,
    realtime_end timestamp with time zone NOT NULL,
    batch_size bigint NOT NULL
);


ALTER TABLE public.round_metrics OWNER TO cmix;

--
-- Name: round_metrics_id_seq; Type: SEQUENCE; Schema: public; Owner: cmix
--

CREATE SEQUENCE public.round_metrics_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.round_metrics_id_seq OWNER TO cmix;

--
-- Name: round_metrics_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cmix
--

ALTER SEQUENCE public.round_metrics_id_seq OWNED BY public.round_metrics.id;


--
-- Name: topologies; Type: TABLE; Schema: public; Owner: cmix
--

CREATE TABLE public.topologies (
    node_id bytea NOT NULL,
    round_metric_id bigint NOT NULL,
    "order" integer NOT NULL
);


ALTER TABLE public.topologies OWNER TO cmix;

--
-- Name: users; Type: TABLE; Schema: public; Owner: cmix
--

CREATE TABLE public.users (
    public_key text NOT NULL
);


ALTER TABLE public.users OWNER TO cmix;

--
-- Name: applications id; Type: DEFAULT; Schema: public; Owner: cmix
--

ALTER TABLE ONLY public.applications ALTER COLUMN id SET DEFAULT nextval('public.applications_id_seq'::regclass);


--
-- Name: node_metrics id; Type: DEFAULT; Schema: public; Owner: cmix
--

ALTER TABLE ONLY public.node_metrics ALTER COLUMN id SET DEFAULT nextval('public.node_metrics_id_seq'::regclass);


--
-- Name: round_errors id; Type: DEFAULT; Schema: public; Owner: cmix
--

ALTER TABLE ONLY public.round_errors ALTER COLUMN id SET DEFAULT nextval('public.round_errors_id_seq'::regclass);


--
-- Name: round_metrics id; Type: DEFAULT; Schema: public; Owner: cmix
--

ALTER TABLE ONLY public.round_metrics ALTER COLUMN id SET DEFAULT nextval('public.round_metrics_id_seq'::regclass);


--
-- Data for Name: applications; Type: TABLE DATA; Schema: public; Owner: cmix
--

COPY public.applications (id, name, url, blurb, location, geo_bin, gps_location, team, network, email, twitter, discord, instagram, medium) FROM stdin;
\.


--
-- Data for Name: node_metrics; Type: TABLE DATA; Schema: public; Owner: cmix
--

COPY public.node_metrics (id, node_id, start_time, end_time, num_pings) FROM stdin;
\.


--
-- Data for Name: nodes; Type: TABLE DATA; Schema: public; Owner: cmix
--

COPY public.nodes (code, sequence, id, server_address, gateway_address, node_certificate, gateway_certificate, date_registered, status, application_id) FROM stdin;
\.


--
-- Data for Name: registration_codes; Type: TABLE DATA; Schema: public; Owner: cmix
--

COPY public.registration_codes (code, remaining_uses) FROM stdin;
\.


--
-- Data for Name: round_errors; Type: TABLE DATA; Schema: public; Owner: cmix
--

COPY public.round_errors (id, round_metric_id, error) FROM stdin;
1	1	test
\.


--
-- Data for Name: round_metrics; Type: TABLE DATA; Schema: public; Owner: cmix
--

COPY public.round_metrics (id, precomp_start, precomp_end, realtime_start, realtime_end, batch_size) FROM stdin;
1	1969-12-31 16:00:00-08	1969-12-31 16:00:00-08	1969-12-31 16:00:00-08	2020-06-09 15:16:50.500443-07	0
\.


--
-- Data for Name: topologies; Type: TABLE DATA; Schema: public; Owner: cmix
--

COPY public.topologies (node_id, round_metric_id, "order") FROM stdin;
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: cmix
--

COPY public.users (public_key) FROM stdin;
\.


--
-- Name: applications_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cmix
--

SELECT pg_catalog.setval('public.applications_id_seq', 1, false);


--
-- Name: node_metrics_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cmix
--

SELECT pg_catalog.setval('public.node_metrics_id_seq', 1, false);


--
-- Name: round_errors_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cmix
--

SELECT pg_catalog.setval('public.round_errors_id_seq', 1, true);


--
-- Name: round_metrics_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cmix
--

SELECT pg_catalog.setval('public.round_metrics_id_seq', 1, false);


--
-- Name: applications applications_pkey; Type: CONSTRAINT; Schema: public; Owner: cmix
--

ALTER TABLE ONLY public.applications
    ADD CONSTRAINT applications_pkey PRIMARY KEY (id);


--
-- Name: node_metrics node_metrics_pkey; Type: CONSTRAINT; Schema: public; Owner: cmix
--

ALTER TABLE ONLY public.node_metrics
    ADD CONSTRAINT node_metrics_pkey PRIMARY KEY (id);


--
-- Name: nodes nodes_pkey; Type: CONSTRAINT; Schema: public; Owner: cmix
--

ALTER TABLE ONLY public.nodes
    ADD CONSTRAINT nodes_pkey PRIMARY KEY (code);


--
-- Name: registration_codes registration_codes_pkey; Type: CONSTRAINT; Schema: public; Owner: cmix
--

ALTER TABLE ONLY public.registration_codes
    ADD CONSTRAINT registration_codes_pkey PRIMARY KEY (code);


--
-- Name: round_errors round_errors_pkey; Type: CONSTRAINT; Schema: public; Owner: cmix
--

ALTER TABLE ONLY public.round_errors
    ADD CONSTRAINT round_errors_pkey PRIMARY KEY (id);


--
-- Name: round_metrics round_metrics_pkey; Type: CONSTRAINT; Schema: public; Owner: cmix
--

ALTER TABLE ONLY public.round_metrics
    ADD CONSTRAINT round_metrics_pkey PRIMARY KEY (id);


--
-- Name: topologies topologies_pkey; Type: CONSTRAINT; Schema: public; Owner: cmix
--

ALTER TABLE ONLY public.topologies
    ADD CONSTRAINT topologies_pkey PRIMARY KEY (node_id, round_metric_id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: cmix
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (public_key);


--
-- Name: uix_nodes_application_id; Type: INDEX; Schema: public; Owner: cmix
--

CREATE UNIQUE INDEX uix_nodes_application_id ON public.nodes USING btree (application_id);


--
-- Name: uix_nodes_id; Type: INDEX; Schema: public; Owner: cmix
--

CREATE UNIQUE INDEX uix_nodes_id ON public.nodes USING btree (id);


--
-- Name: node_metrics node_metrics_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: cmix
--

ALTER TABLE ONLY public.node_metrics
    ADD CONSTRAINT node_metrics_node_id_fkey FOREIGN KEY (node_id) REFERENCES public.nodes(id);


--
-- Name: nodes nodes_application_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: cmix
--

ALTER TABLE ONLY public.nodes
    ADD CONSTRAINT nodes_application_id_fkey FOREIGN KEY (application_id) REFERENCES public.applications(id);


--
-- Name: round_errors round_errors_round_metric_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: cmix
--

ALTER TABLE ONLY public.round_errors
    ADD CONSTRAINT round_errors_round_metric_id_fkey FOREIGN KEY (round_metric_id) REFERENCES public.round_metrics(id);


--
-- Name: topologies topologies_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: cmix
--

ALTER TABLE ONLY public.topologies
    ADD CONSTRAINT topologies_node_id_fkey FOREIGN KEY (node_id) REFERENCES public.nodes(id);


--
-- Name: topologies topologies_round_metric_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: cmix
--

ALTER TABLE ONLY public.topologies
    ADD CONSTRAINT topologies_round_metric_id_fkey FOREIGN KEY (round_metric_id) REFERENCES public.round_metrics(id);


--
-- PostgreSQL database dump complete
--
