const configureKnex = require('knex');
const objection = require('objection');
const visibilityPlugin = require('objection-visibility').default;
const remoteConfig = require('../knexfile.remote.js');

const remote = configureKnex(remoteConfig);

objection.Model.knex(remote);

module.exports = {
  knex: remote,
  Model: visibilityPlugin(objection.Model),
};
