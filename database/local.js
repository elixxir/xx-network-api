const configureKnex = require('knex');
const objection = require('objection');
const pg = require('pg');
const visibilityPlugin = require('objection-visibility').default;
const localConfig = require('../knexfile.js');

pg.types.setTypeParser(pg.types.builtins.NUMERIC, (v) => parseFloat(v));

const local = configureKnex(localConfig);

objection.Model.knex(local);

module.exports = {
  knex: local,
  Model: visibilityPlugin(objection.Model),
};
