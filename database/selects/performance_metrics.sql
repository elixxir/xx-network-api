WITH precomp_rounds_failed AS (
  SELECT
    t.node_id,
    COUNT(DISTINCT re.round_metric_id)
  FROM topologies t
  RIGHT JOIN round_metrics rm
  ON rm.id = t.round_metric_id
  RIGHT JOIN round_errors re
  ON t.round_metric_id = re.round_metric_id
  WHERE rm.precomp_start BETWEEN ? and ?
  AND rm.precomp_end = '1970-01-01'::date
  GROUP BY t.node_id
), realtime_rounds_failed AS (
  SELECT
    t.node_id,
    COUNT(DISTINCT re.round_metric_id)
  FROM topologies t
  RIGHT JOIN round_metrics rm
  ON rm.id = t.round_metric_id
  RIGHT JOIN round_errors re
  ON t.round_metric_id = re.round_metric_id
  WHERE rm.precomp_start BETWEEN ? and ?
  AND rm.precomp_end != '1970-01-01'::date
  GROUP BY t.node_id
), participated_rounds AS (
  SELECT
    node_id,
    COUNT(DISTINCT round_metric_id)
  FROM topologies t
  JOIN round_metrics rm
  ON rm.id = t.round_metric_id
  WHERE rm.precomp_start BETWEEN ? and ?
  GROUP BY node_id
), uptime (node_id, node_metric_count, uptime_metric_count) AS (
  SELECT
  DISTINCT nm.node_id,
  COUNT(*),
  SUM(CASE WHEN num_pings > 0 THEN 1 ELSE 0 END)
  FROM node_metrics nm
  WHERE nm.end_time BETWEEN ? and ?
  GROUP BY nm.node_id
), round_performance AS (
  SELECT
    DISTINCT node_id,
    extract('epoch' from AVG(r.realtime_end - r.realtime_start)) AS realtime_avg_seconds,
    extract('epoch' from AVG(r.precomp_end - r.precomp_start)) AS precomp_avg_seconds
  FROM topologies t
    RIGHT JOIN round_metrics r
    ON r.id = t.round_metric_id
  WHERE NOT EXISTS (SELECT round_metric_id from round_errors re WHERE re.round_metric_id = r.id)
  AND r.realtime_start != '1970-01-01'::date
  AND r.realtime_end != '1970-01-01'::date
  AND r.precomp_start != '1970-01-01'::date
  AND r.precomp_end != '1970-01-01'::date
  AND r.realtime_end BETWEEN ? AND ?
  GROUP BY node_id
)
SELECT
  encode(n.id, 'base64') AS node_id,
  (COALESCE(rfr.count, 0) + COALESCE(pfr.count, 0))::integer AS rounds_failed,
  COALESCE(rfr.count::integer, 0) AS realtime_rounds_failed,
  COALESCE(pfr.count::integer, 0) AS precomp_rounds_failed,
  COALESCE(pr.count::integer, 0) AS rounds_participated,
  COALESCE(u.uptime_metric_count::integer, 0) AS uptime_metric_count,
  COALESCE(u.node_metric_count::integer, 0) AS node_metric_count,
  rp.realtime_avg_seconds AS realtime_avg_seconds,
  rp.precomp_avg_seconds AS precomp_avg_seconds,
  ? AS interval_start,
  ? AS interval_end
FROM nodes n
LEFT JOIN realtime_rounds_failed rfr
ON rfr.node_id = n.id
LEFT JOIN precomp_rounds_failed pfr
ON pfr.node_id = n.id
LEFT JOIN participated_rounds pr
ON pr.node_id = n.id
JOIN uptime u
ON u.node_id = n.id
LEFT JOIN round_performance rp
ON rp.node_id = n.id

