WITH precomp_rounds_failed AS (
  SELECT
    COUNT(DISTINCT re.round_metric_id) AS precomp_rounds_failed
  FROM topologies t
  RIGHT JOIN round_metrics rm
  ON rm.id = t.round_metric_id
  RIGHT JOIN round_errors re
  ON t.round_metric_id = re.round_metric_id
  WHERE rm.precomp_start BETWEEN ? and ?
  AND rm.precomp_end = '1970-01-01'::date
), realtime_rounds_failed AS (
  SELECT
    COUNT(DISTINCT re.round_metric_id) as realtime_rounds_failed
  FROM topologies t
  RIGHT JOIN round_metrics rm
  ON rm.id = t.round_metric_id
  RIGHT JOIN round_errors re
  ON t.round_metric_id = re.round_metric_id
  WHERE rm.precomp_start BETWEEN ? and ?
  AND rm.precomp_end != '1970-01-01'::date
), other_round_metrics AS (
  SELECT
    SUM(r.batch_size) as batch_size_sum,
    COUNT(r.id) as successful_rounds,
    EXTRACT('epoch' from AVG(r.realtime_end - r.realtime_start)) AS realtime_avg_seconds,
    EXTRACT('epoch' from AVG(r.precomp_end - r.precomp_start)) AS precomp_avg_seconds
  FROM
    round_metrics r
  WHERE NOT EXISTS (SELECT round_metric_id from round_errors re WHERE re.round_metric_id = r.id)
  AND r.precomp_start BETWEEN ? AND ?
)
SELECT
  COALESCE(r.batch_size_sum, 0) as batch_size_sum,
  COALESCE(r.successful_rounds, 0) as successful_rounds,
  COALESCE(prf.precomp_rounds_failed, 0) as precomp_rounds_failed,
  COALESCE(rrf.realtime_rounds_failed, 0) as realtime_rounds_failed,
  r.realtime_avg_seconds,
  r.precomp_avg_seconds,
  ? AS interval_start,
  ? AS interval_end
FROM
other_round_metrics r
LEFT JOIN precomp_rounds_failed prf
ON true
LEFT JOIN realtime_rounds_failed rrf
ON true
