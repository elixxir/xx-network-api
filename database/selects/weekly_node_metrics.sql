SELECT
  *
FROM
  weekly_node_performance_metrics
ORDER BY node_id, week DESC
