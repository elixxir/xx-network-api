const local = require('./local');
const remote = require('./remote');

module.exports = {
  local,
  remote,
};
