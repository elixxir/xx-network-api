const camelcaseKeys = require('camelcase-keys');
const NodeCache = require('node-cache');
const { omit, get: safeGet, pick } = require('lodash');
const fs = require('fs');
const { knex: local } = require('../database/local');
const { sortByDate } = require('../utils');
const Node = require('../models/node');

const cache = new NodeCache();

const nodeStatuses = fs.readFileSync('./database/selects/node_statuses.sql').toString();
const nodeWhois = 'SELECT * FROM node_whois';

const fetchLocally = async (rawSql, ...args) => {
  const { rows } = await local.raw(rawSql, ...args);
  return rows.map(camelcaseKeys);
};

const fetchAll = async ({ refreshCache } = { refreshCache: false }) => {
  let nodes = cache.get('nodes');

  if (!nodes || refreshCache) {
    const [fetchedNodes, statuses, latestMonthly, nodeWhoises] = await Promise.all([
      Node.query().select('nodes.id').withGraphJoined('application').modify('filterActive'),
      fetchLocally(nodeStatuses),
      fetchLocally(`
        SELECT
          DISTINCT ON (node_id) node_id,
          month,
          uptime,
          failure_rate,
          total_successful_rounds,
          total_realtime_rounds_failed,
          total_precomp_rounds_failed
        FROM
          monthly_node_performance_metrics
        ORDER BY node_id, month DESC
      `),
      fetchLocally(nodeWhois),
    ]);

    nodes = fetchedNodes
      .map((node) => {
        const n = omit(node.toJSON(), 'application');
        const a = node.application.toJSON();
        const extra = {
          ...statuses.find(({ nodeId }) => nodeId === n.id),
          ...nodeWhoises.find(({ nodeId }) => nodeId === n.id),
        };
        const description = a.blurb;

        let monthly = latestMonthly
          .filter(({ nodeId }) => nodeId === n.id)
          .map((m) => ({
            ...omit(m, ['nodeId', 'month']),
            month: m.month.toISOString(),
          }));

        monthly = sortByDate(monthly, 'month');

        return {
          ...pick(a, [
            'name',
            'url',
            'location',
            'geoBin',
            'gpsLocation',
            'team',
            'network',
            'email',
            'twitter',
            'discord',
            'instagram',
            'medium',
            'other',
            'forum',
          ]),
          ...pick(n, ['id', 'base64Url']),
          applicationId: a ? parseInt(a.id, 10) : undefined,
          description,
          uptime: safeGet(monthly, '[0].uptime') || 0,
          roundFailureAvg: safeGet(monthly, '[0].failureRate') || 0,
          status: (extra && extra.status) ? extra.status : 'unknown',
          whois: (extra && extra.whois) ? extra.whois : undefined,
        };
      }).filter(({ status }) => ['online', 'offline', 'error'].includes(status));

    cache.set('nodes', nodes);
  }

  return nodes;
};

const get = async (_req, res) => {
  const nodes = await fetchAll();
  res.json({ nodes });
};

const findNode = async (id) => {
  const nodes = await fetchAll();

  const lc = id.toString().toLowerCase();
  return nodes.find(({
    base64Url,
    name,
    id: nodeId,
  }) => base64Url.toLowerCase() === lc || name.toLowerCase() === lc || nodeId.toLowerCase() === lc);
};

const find = async (req, res, next) => {
  const { id } = req.params;

  const node = await findNode(id);

  if (!node) {
    return next();
  }

  const instance = Node.fromJson({ ...node, id: Buffer.from(node.id, 'base64') });

  const [rounds, metrics, monthlies, weeklies] = await Promise.all([
    instance.$relatedQuery('rounds')
      .withGraphFetched('errors')
      .modify('lastTwoDays')
      .orderBy('realtimeEnd', 'desc')
      .limit(100),
    instance.$relatedQuery('metrics')
      .modify('lastTwoDays')
      .orderBy('endTime', 'desc')
      .limit(100),
    fetchLocally(`
        SELECT
          *
        FROM
          monthly_node_performance_metrics
        WHERE node_id = ?
        ORDER BY node_id, month DESC
      `, node.id),
    fetchLocally(`
        SELECT
          *
        FROM
          weekly_node_performance_metrics
        WHERE node_id = ?
        ORDER BY node_id, week DESC
    `, node.id),
  ]);

  const monthly = monthlies
    .filter(({ nodeId }) => nodeId === node.id)
    .map((m) => ({
      ...omit(m, ['nodeId', 'month']),
      month: m.month.toISOString(),
    }));

  node.monthlyMetrics = sortByDate(monthly, 'month');

  const weekly = weeklies
    .filter(({ nodeId }) => nodeId === node.id)
    .map((w) => ({
      ...omit(w, ['nodeId', 'week']),
      week: w.week.toISOString(),
    }));

  node.weeklyMetrics = sortByDate(weekly, 'week');

  return res.json({
    node,
    rounds: (rounds && rounds.length) ? rounds.map((r) => r.toJSON()) : [],
    metrics: (metrics && metrics.length) ? metrics.map((m) => m.toJSON()) : [],
  });
};

const fetchActiveNodeCount = async () => parseInt(
  (await Node.query().modify('filterActive').count().first()).count, 10,
);

const cacheExpiryHandlers = [
  fetchAll,
];

const refreshCache = () => Promise.all(
  cacheExpiryHandlers.map((fn) => fn({ refreshCache: true })),
);

setInterval(refreshCache, 5 * 1000);

module.exports = {
  get,
  find,
  findNode,
  initialize: refreshCache(),
  fetchActiveNodeCount,
};
