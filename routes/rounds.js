
const camelcaseKeys = require('camelcase-keys');
const base64Url = require('base64url');
const NodeCache = require('node-cache');
const queryable = require('queryable-promise');
const { omit, sortBy } = require('lodash');
const { eventBus } = require('../scripts/refresh_views.js');
const { knex: remoteDb } = require('../database/remote');
const { knex } = require('../database/remote');
const RoundMetrics = require('../models/round_metrics');

const cache = new NodeCache();

const fetchRemotely = async (rawSql, args) => {
  const { rows } = await remoteDb.raw(rawSql, args);
  return rows.map(camelcaseKeys);
};

const { ref } = RoundMetrics;

const roundMapper = (round) => (round ? ({
  ...omit(round.toJSON(), 'topologies'),
  nodes: sortBy(round.topologies.map((t) => ({
    order: t.order,
    ...omit(t.node.toJSON(), 'application'),
    ...omit(t.node.application, 'id'),
  })), 'order'),
}) : undefined);

const findRound = async (id) => {
  const round = await RoundMetrics.query()
    .where(ref('round_metrics.id'), id)
    .withGraphFetched('[topologies.node(defaultSelects).application,errors]')
    .limit(1)
    .first()
    .catch(() => { /* do nothing */ });

  return round ? roundMapper(round) : undefined;
};

const find = async (req, res) => {
  const { id } = req.params;
  const round = await findRound(id);

  return round
    ? res.json({ round })
    : res.sendStatus(404);
};

const simpleRoundMapper = (round) => (
  round
    ? camelcaseKeys({
      ...omit(round.toJSON(), 'topologies'),
      nodes: round.topologies.map((t) => (
        {
          id: t.nodeId.toString('base64'),
          base64Url: base64Url(t.nodeId),
          order: t.order,
        }
      )),
    })
    : undefined);

const fetchAllRounds = async (req) => {
  const result = await RoundMetrics.query()
    .withGraphFetched('[topologies,errors]')
    .orderBy(ref('realtimeEnd'), 'desc')
    .limit(req.query.limit);
  return { rounds: result.map(simpleRoundMapper), total: result.total };
};

const fetchLastCompletedRound = async () => (
  await RoundMetrics.query().orderBy('id', 'desc').limit(1).first()
) || null;

let totalTask;
const fetchTotalSuccessfulRounds = async ({ refreshCache } = { refreshCache: false }) => {
  let count = cache.get('total_successful_rounds');
  if (!count || refreshCache) {
    if (!totalTask || !totalTask.isPending()) {
      totalTask = queryable(fetchRemotely(`
        SELECT (select count(*) FROM round_metrics) - (SELECT count(DISTINCT round_metric_id) FROM round_errors) as count
      `));
    }
    const rows = await totalTask;
    count = rows ? parseInt(rows[0].count, 10) : null;
    eventBus.emit('total_successful_rounds_updated', count);
    cache.set('total_successful_rounds', count);
  }
  return count;
};

const fetchRoundsAfter = async (id) => {
  const rounds = await RoundMetrics.query()
    .withGraphFetched('[topologies.node(defaultSelects).application(locationSelects),errors]')
    .where(
      'realtimeEnd',
      '>',
      (q) => q.select('realtimeEnd').from('round_metrics').where('id', id).limit(1),
    ).orderBy('realtimeEnd', 'desc')
    .limit(200);

  return rounds.map(roundMapper);
};

const get = async (req, res) => {
  const { rounds, total } = await fetchAllRounds(req);
  res.json({ rounds, total });
};

// We partition this over a shorter interval to speed up the query
// If anyone needs more results then increase the interval below.
const interval = '10 minutes';
const whereClauses = {
  near: 'ABS(cte.row_number - (SELECT row_number FROM cte c WHERE c.id = ?)) <= ?',
  after: 'cte.row_number - (SELECT row_number FROM cte c WHERE c.id = ?) BETWEEN 0 AND ?',
  before: '(SELECT row_number FROM cte c WHERE c.id = ?) - cte.row_number BETWEEN 0 AND ?',
};

const fetchRoundIdsNearIdQuery = (id, verb = 'near', delta = 15) => {
  const where = whereClauses[verb];
  return knex.raw(`
    WITH current as
      (SELECT realtime_end FROM round_metrics rm2 WHERE rm2.id = ?),
    cte AS (SELECT
      id,
      row_number() OVER (order by rm.realtime_end)
    FROM
      round_metrics rm, current
    WHERE rm.realtime_end
      BETWEEN current.realtime_end - interval '${interval}'
      AND current.realtime_end + interval '${interval}'
    ) SELECT cte.* FROM cte, current
    WHERE ${where}
    ORDER BY cte.row_number;
  `, [id, id, delta]);
};

const near = async (req, res) => {
  const { id, verb } = req.params;
  const { delta } = req.query;
  const result = (await fetchRoundIdsNearIdQuery(id, verb, delta)) || {};

  res.json({ roundIds: (result.rows || []).map(({ id: i }) => i) });
};

const cacheExpiryHandlers = [
  fetchTotalSuccessfulRounds,
];

const refreshCache = () => Promise.all(
  cacheExpiryHandlers.map((fn) => fn({ refreshCache: true })),
);

setInterval(refreshCache, 10 * 1000);

module.exports = {
  get,
  find,
  findRound,
  near,
  fetchLastCompletedRound,
  fetchTotalSuccessfulRounds,
  fetchRoundsAfter,
};
