/* eslint-disable no-await-in-loop */
const SSE = require('express-sse');
const { eventBus } = require('../scripts/refresh_views.js');
const { fetchNetwork } = require('./network');
const { knex: local } = require('../database/local');

const { fetchLastCompletedRound, fetchRoundsAfter } = require('./rounds');

const sse = new SSE();
const get = sse.init;

const maxBy = (items, fn) => items
  .map((x) => [x, fn(x)])
  .reduce((max, x) => (x[1] > max[1] ? x : max))[0];

const getLatestRound = (rounds) => maxBy(
  rounds,
  ({ realtimeEnd }) => new Date(realtimeEnd).getTime(),
);

const fetchAndSendNewRounds = async (roundId) => {
  const rounds = await fetchRoundsAfter(roundId);
  rounds.forEach((r) => sse.send(r, 'round'));
  return rounds.length === 0 ? roundId : getLatestRound(rounds).id;
};

eventBus.on('total_successful_rounds_updated', async (total) => {
  sse.send(total, 'total_successful_rounds');
});

eventBus.on('node_statuses_updated', async () => {
  const { rows } = await local.raw('SELECT node_id, status FROM node_statuses');
  const statuses = rows.reduce((acc, { node_id: nid, status }) => {
    acc[nid] = status;
    return acc;
  }, {});
  sse.send(statuses, 'node_statuses_updated');
});

eventBus.on('network_metrics_updated', async () => {
  const network = await fetchNetwork();
  sse.send(network, 'network_metrics_updated');
});

let lastRoundId;
(async () => {
  lastRoundId = (await fetchLastCompletedRound()).id;
  const roundSSERecursiveMainLoop = async () => {
    setTimeout(async () => {
      try {
        lastRoundId = await fetchAndSendNewRounds(lastRoundId);
      } catch (err) {
        console.error(err.message);
      }
      roundSSERecursiveMainLoop();
    }, process.env.ROUND_REFRESH_RATE_MS || 500);
  };
  roundSSERecursiveMainLoop();
})();


module.exports = {
  get,
};
