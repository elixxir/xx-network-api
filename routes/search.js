const { findNode } = require('./nodes');
const { findRound } = require('./rounds');

const search = async (req, res, next) => {
  const { q } = req.query;
  if (!q) { return next(); }

  const results = await Promise.all([
    findNode(q),
    findRound(q),
  ]);

  if (results.every((r) => !r)) {
    return next();
  }

  const [node, round] = results;

  return res.json({ nodes: node ? [node] : undefined, rounds: round ? [round] : undefined });
};

module.exports = {
  search,
};
