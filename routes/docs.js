const swaggerJsdoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');
const definition = require('../openapi.json');

const options = {
  definition,
  apis: ['./routes/index.js'],
};

if (process.env.NODE_ENV !== 'production') {
  definition.servers.unshift({
    url: 'https://dashboard-api-staging.xx.network/v1',
  });
}

const specs = swaggerJsdoc(options);

module.exports = {
  use: [
    swaggerUi.serve,
    swaggerUi.setup(specs),
  ],
};
