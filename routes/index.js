const paginate = require('express-paginate');
const routes = require('express').Router();
const network = require('./network');
const nodes = require('./nodes');
const rounds = require('./rounds');
const sse = require('./sse');
const { search } = require('./search');
const docs = require('./docs');

const notFound = (req, res) => res.status(404).json({ error: 'Not Found' });

routes.use((req, _res, next) => {
  if (req.query.limit <= 10) {
    req.query.limit = 10;
  }
  next();
});

routes.use(paginate.middleware(50, 100));

routes.get('/network', network.get);
routes.get('/nodes', nodes.get);
routes.get('/nodes/:id', nodes.find);
routes.get('/rounds', rounds.get);
routes.get('/rounds/:id(\\d+)', rounds.find);
routes.get('/rounds/:verb(near|after|before)/:id(\\d+)', rounds.near);
routes.get('/search', search);
routes.get('/sse', sse.get);
routes.use('/docs', ...docs.use);
routes.get('/*', notFound);

module.exports = {
  routes,
  initialize: () => Promise.all([
    nodes.initialize,
    network.initialize,
  ]),
};
