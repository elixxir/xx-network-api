const camelcaseKeys = require('camelcase-keys');
const { knex: localDb } = require('../database/local');
const { fetchLastCompletedRound, fetchTotalSuccessfulRounds } = require('./rounds');
const { fetchActiveNodeCount } = require('./nodes');
const { average, sum } = require('../utils');

const fetchLocally = async (rawSql, args) => {
  const { rows } = await localDb.raw(rawSql, args);
  return rows.map(camelcaseKeys);
};

const fetchPerformance = () => fetchLocally(`
  SELECT
  ROUND(CAST(batch_size_sum / extract('epoch' from interval_end - interval_start) as numeric), 2) AS tps,
  successful_rounds,
  precomp_rounds_failed,
  realtime_rounds_failed,
  realtime_avg_seconds as latency,
  precomp_avg_seconds,
  DATE_TRUNC('hour', timezone('GMT', interval_start)) AS hour,
  interval_start,
  interval_end
  FROM network_performance_metrics
  WHERE interval_end < NOW()
  ORDER BY hour DESC LIMIT 24;
`);

const fetchNetwork = async () => {
  const [
    performance,
    lastCompletedRound,
    activeNodeCount,
    totalSuccessfulRounds,
  ] = await Promise.all([
    fetchPerformance(),
    fetchLastCompletedRound(),
    fetchActiveNodeCount(),
    fetchTotalSuccessfulRounds(),
  ]);

  const meanTps = average(performance.map(({ tps }) => parseFloat(tps)));
  const meanLatency = average(performance.map(({ latency }) => parseFloat(latency))) || 'N/A';
  const successful = sum(performance.map(({ successfulRounds }) => successfulRounds || 0));
  const precompFails = sum(performance.map(({ precompRoundsFailed }) => precompRoundsFailed || 0));
  const realtimeFails = sum(performance.map(
    ({ realtimeRoundsFailed }) => realtimeRoundsFailed || 0,
  ));

  const successRate = (((successful - precompFails - realtimeFails) / successful) * 100).toFixed(2);
  const realtimeSuccessRate = (((successful - realtimeFails) / successful) * 100).toFixed(2);

  return {
    registeredNodes: 167,
    serverVersion: '2.2.0',
    gatewayVersion: '2.3.0',
    successRate,
    realtimeSuccessRate,
    performance: performance.map(({
      tps,
      latency,
      hour,
      successfulRounds,
      precompRoundsFailed,
      realtimeRoundsFailed,
      intervalStart,
      intervalEnd,
    }) => ({
      precompRoundsFailed: parseInt(precompRoundsFailed, 10),
      realtimeRoundsFailed: parseInt(realtimeRoundsFailed, 10),
      successfulRounds: parseInt(successfulRounds, 10),
      tps: parseFloat(tps),
      latency: parseFloat(latency),
      hour,
      intervalStart,
      intervalEnd,
    })),
    lastCompletedRound,
    activeNodeCount,
    totalSuccessfulRounds,
    meanTps,
    meanLatency,
  };
};

const get = async (_req, res) => {
  const network = await fetchNetwork();
  res.json({ network });
};

module.exports = {
  get,
  fetchNetwork,
  fetchLastCompletedRound,
};
