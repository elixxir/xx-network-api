const fs = require('fs');

const sql = fs.readFileSync('./database/views/monthly/node_uptimes.sql').toString();

exports.up = (knex) => knex.raw(`
  CREATE MATERIALIZED VIEW IF NOT EXISTS node_uptimes
  AS SELECT * FROM dblink(
    '${process.env.REMOTE_DB_URL}',
    '${sql.replace(/'/g, '\'\'')}'
  ) as t(node_id text, month timestamp, uptime numeric)
  WITH NO DATA;

  CREATE UNIQUE INDEX node_uptimes_node_id_idx ON node_uptimes (node_id, month);
  CREATE INDEX node_uptimes_uptime_idx ON node_uptimes (uptime);
`);

exports.down = (knex) => knex.raw('DROP MATERIALIZED VIEW IF EXISTS node_uptimes');
