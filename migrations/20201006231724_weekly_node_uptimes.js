const fs = require('fs');

const sql = fs.readFileSync('./database/views/weekly/node_uptimes.sql').toString();
const table = 'weekly_node_uptimes';

exports.up = (knex) => knex.raw(`
  CREATE MATERIALIZED VIEW IF NOT EXISTS ${table}
  AS SELECT * FROM dblink(
    '${process.env.REMOTE_DB_URL}',
    '${sql.replace(/'/g, '\'\'')}'
  ) as t(node_id text, week timestamp, uptime numeric)
  WITH NO DATA;

  CREATE UNIQUE INDEX  ${table}_node_id_idx ON  ${table} (node_id, week);
  CREATE INDEX  ${table}_uptime_idx ON  ${table} (uptime);
`);

exports.down = (knex) => knex.raw(`DROP MATERIALIZED VIEW IF EXISTS ${table}`);
