exports.up = (knex) => knex.schema.createTable('node_performance_metrics', (table) => {
  table.string('node_id');
  table.decimal('realtime_avg_seconds', 14, 8).nullable().defaultTo(null);
  table.decimal('precomp_avg_seconds', 14, 8).nullable().defaultTo(null);
  table.integer('node_metric_count').notNullable().defaultTo(0);
  table.integer('uptime_metric_count').notNullable().defaultTo(0);
  table.integer('rounds_participated').notNullable().defaultTo(0);
  table.integer('rounds_failed').notNullable().defaultTo(0);
  table.timestamp('interval_start');
  table.timestamp('interval_end');

  table.index('node_id');
  table.index('interval_start');
  table.index('interval_end');

  table.unique(['node_id', 'interval_end']);
  table.unique(['node_id', 'interval_start']);
});

exports.down = (knex) => knex.schema.dropTable('node_performance_metrics');
