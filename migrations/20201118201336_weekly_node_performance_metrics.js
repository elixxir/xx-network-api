
const table = 'weekly_node_performance_metrics';

exports.up = (knex) => knex.raw(`
  CREATE MATERIALIZED VIEW IF NOT EXISTS ${table}
  AS WITH max_metric_counts AS (
    SELECT
      max(total_count) as max,
      week
    FROM (
      SELECT
        node_id,
        SUM(node_metric_count) as total_count,
        DATE_TRUNC('week', timezone('GMT', interval_start)) as week
      FROM node_performance_metrics
      GROUP BY node_id, week
    ) m
    GROUP BY week
  ), performance_metrics AS (
    SELECT
    node_id,
    DATE_TRUNC('week', timezone('GMT', interval_start)) AS week,
    ROUND(CAST(SUM(rounds_failed) as numeric) / NULLIF(CAST(SUM(rounds_participated) as numeric), 0) * 100, 2)  as failure_rate,
    ROUND(AVG(realtime_avg_seconds), 3) AS realtime_avg_seconds,
    ROUND(AVG(precomp_avg_seconds), 3) AS precomp_avg_seconds,
    SUM(uptime_metric_count) AS total_metric_count
    FROM node_performance_metrics
    GROUP BY node_id, week
  )
  SELECT
    pm.node_id,
    pm.week,
    pm.failure_rate,
    ROUND(
      (pm.total_metric_count::numeric / mmc.max::numeric) * 100,
      2
    ) as uptime,
    pm.realtime_avg_seconds,
    pm.precomp_avg_seconds
  FROM performance_metrics pm
  JOIN max_metric_counts mmc
  ON mmc.week = pm.week;

  CREATE UNIQUE INDEX ${table}_node_id_idx ON ${table} (node_id, week);
`);

exports.down = (knex) => knex.raw(`DROP MATERIALIZED VIEW IF EXISTS ${table}`);
