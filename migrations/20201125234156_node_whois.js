
exports.up = (knex) => knex.schema.createTable('node_whois', (table) => {
  table.string('node_id').primary();
  table.string('whois').nullable();
  table.unique('node_id');
  table.timestamps();
});

exports.down = (knex) => knex.schema.dropTable('node_whois');
