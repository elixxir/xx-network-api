exports.up = async (knex) => {
  await knex.raw('DELETE FROM network_performance_metrics WHERE 1 = 1;');
  await knex.schema.table('network_performance_metrics', (table) => {
    table.integer('successful_rounds').nullable().defaultTo(null);
    table.decimal('realtime_avg_seconds', 14, 8).nullable().defaultTo(null);
    table.decimal('precomp_avg_seconds', 14, 8).nullable().defaultTo(null);
  });
};

exports.down = (knex) => knex.schema.table('network_performance_metrics', (table) => {
  table.dropColumn('successful_rounds');
  table.dropColumn('realtime_avg_seconds');
  table.dropColumn('precomp_avg_seconds');
});
