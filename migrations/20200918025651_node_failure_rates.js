const fs = require('fs');

const sql = fs.readFileSync('./database/views/monthly/node_failure_rates.sql').toString();

exports.up = (knex) => knex.raw(`
  CREATE MATERIALIZED VIEW IF NOT EXISTS node_failure_rates
  AS SELECT * FROM dblink(
    '${process.env.REMOTE_DB_URL}',
    '${sql.replace(/'/g, '\'\'')}'
  ) as t(node_id text, month timestamp, failure_rate numeric)
  WITH NO DATA;

  CREATE UNIQUE INDEX node_failure_rates_node_id_idx ON node_failure_rates (node_id, month);
`);

exports.down = (knex) => knex.raw('DROP MATERIALIZED VIEW IF EXISTS node_failure_rates');
