const table = 'weekly_node_performance_metrics';

exports.up = (knex) => knex.raw(`
  DROP MATERIALIZED VIEW  IF EXISTS ${table};
  CREATE MATERIALIZED VIEW ${table} AS
  WITH max_metric_counts AS (
    SELECT
      max(total_count) as max,
      week
    FROM (
      SELECT
        node_id,
        SUM(node_metric_count) as total_count,
        DATE_TRUNC('week', timezone('GMT', interval_start)) as week
      FROM node_performance_metrics
      GROUP BY node_id, week
    ) m
    GROUP BY week
  ), performance_metrics AS (
    SELECT
      node_id,
      DATE_TRUNC('week', timezone('GMT', interval_start)) AS week,
      ROUND(CAST(SUM(rounds_failed) as numeric) / NULLIF(CAST(SUM(rounds_participated) as numeric), 0) * 100, 2)  as failure_rate,
      SUM(uptime_metric_count) AS total_node_metric_count,
      SUM(rounds_participated) as total_rounds_participated,
      SUM(rounds_participated) - SUM(rounds_failed) as total_successful_rounds,
      SUM(rounds_failed) as total_rounds_failed,
      SUM(precomp_rounds_failed) as total_precomp_rounds_failed,
      SUM(realtime_rounds_failed) as total_realtime_rounds_failed
    FROM node_performance_metrics
    GROUP BY node_id, week
  ), weighted_performance_metrics AS (
    SELECT
      node_id,
      week,
      SUM(realtime_avg_seconds * weight) AS realtime_avg_seconds,
      SUM(precomp_avg_seconds * weight) AS precomp_avg_seconds
    FROM (
      SELECT
        t.node_id,
        t.week,
        successful_rounds / NULLIF(pm.total_successful_rounds::numeric, 0) as weight,
        realtime_avg_seconds,
        precomp_avg_seconds
        FROM (
          SELECT
            node_id,
            date_trunc('week', timezone('GMT', interval_end)) as week,
            realtime_avg_seconds,
            precomp_avg_seconds,
            rounds_participated - rounds_failed as successful_rounds
          FROM node_performance_metrics npm
        ) t
        JOIN performance_metrics pm
        ON pm.node_id = t.node_id
        AND pm.week = t.week
      ) z
    GROUP BY node_id, week
  )
  SELECT
    pm.node_id,
    pm.week,
    pm.failure_rate,
    pm.total_rounds_participated,
    pm.total_successful_rounds,
    pm.total_precomp_rounds_failed,
    pm.total_realtime_rounds_failed,
    pm.total_rounds_failed,
    ROUND(
      (pm.total_node_metric_count::numeric / mmc.max::numeric) * 100,
      2
    ) as uptime,
    ROUND(wpm.realtime_avg_seconds, 3) as realtime_avg_seconds,
    ROUND(wpm.precomp_avg_seconds, 3) as precomp_avg_seconds
  FROM performance_metrics pm
  JOIN max_metric_counts mmc
  ON mmc.week = pm.week
  JOIN weighted_performance_metrics wpm
  ON pm.node_id = wpm.node_id AND pm.week = wpm.week;

  CREATE UNIQUE INDEX IF NOT EXISTS ${table}_idx ON ${table} (node_id, week);
  CREATE INDEX ${table}_week_idx ON ${table} (week);
`);

exports.down = () => {};
