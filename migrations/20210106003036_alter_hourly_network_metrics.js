const table = 'hourly_network_metrics';

exports.up = (knex) => knex.raw(`DROP MATERIALIZED VIEW IF EXISTS ${table}`);

exports.down = (knex) => knex.raw(`DROP MATERIALIZED VIEW IF EXISTS ${table}`);
