exports.up = (knex) => knex.schema.createTable('network_performance_metrics', (table) => {
  table.bigInteger('batch_size_sum').defaultTo(0);
  table.timestamp('interval_start');
  table.timestamp('interval_end');
  table.unique('interval_start');
  table.unique('interval_end');
});

exports.down = (knex) => knex.schema.dropTable('network_performance_metrics');
