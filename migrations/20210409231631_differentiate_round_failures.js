exports.up = (knex) => knex.schema.table('node_performance_metrics', (t) => {
  t.integer('precomp_rounds_failed').defaultTo(0);
  t.integer('realtime_rounds_failed').defaultTo(0);
});

exports.down = (knex) => knex.schema.table('node_performance_metrics', (t) => {
  t.dropColumn('precomp_rounds_failed');
  t.dropColumn('realtime_rounds_failed');
});
