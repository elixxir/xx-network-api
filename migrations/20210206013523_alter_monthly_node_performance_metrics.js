const table = 'monthly_node_performance_metrics';

exports.up = (knex) => knex.raw(`
  DROP MATERIALIZED VIEW IF EXISTS ${table};
  CREATE MATERIALIZED VIEW IF NOT EXISTS ${table}
  AS WITH max_metric_counts AS (
    SELECT
      max(total_count) as max,
      month
    FROM (
      SELECT
        node_id,
        SUM(node_metric_count) as total_count,
        DATE_TRUNC('month', timezone('GMT', interval_start)) as month
      FROM node_performance_metrics
      GROUP BY node_id, month
    ) m
    GROUP BY month
  ), performance_metrics AS (
    SELECT
      node_id,
      DATE_TRUNC('month', timezone('GMT', interval_start)) AS month,
      ROUND(CAST(SUM(rounds_failed) as numeric) / NULLIF(CAST(SUM(rounds_participated) as numeric), 0) * 100, 2)  as failure_rate,
      SUM(uptime_metric_count) AS total_node_metric_count,
      SUM(rounds_participated) as total_rounds_participated,
      SUM(rounds_participated) - SUM(rounds_failed) as total_successful_rounds
    FROM node_performance_metrics
    GROUP BY node_id, month
  ), weighted_performance_metrics AS (
    SELECT
      node_id,
      month,
      SUM(realtime_avg_seconds * weight) AS realtime_avg_seconds,
      SUM(precomp_avg_seconds * weight) AS precomp_avg_seconds,
      SUM(weight) as total_weight
    FROM (
      SELECT
        t.node_id,
        t.month,
        successful_rounds / NULLIF(pm.total_successful_rounds::numeric, 0) as weight,
        realtime_avg_seconds,
        precomp_avg_seconds
        FROM (
          SELECT
            node_id,
            DATE_TRUNC('month', timezone('GMT', interval_end)) as month,
            realtime_avg_seconds,
            precomp_avg_seconds,
            rounds_participated - rounds_failed as successful_rounds
          FROM node_performance_metrics npm
        ) t
        JOIN performance_metrics pm
        ON pm.node_id = t.node_id
        AND pm.month = t.month
      ) z
    GROUP BY node_id, month
  )
  SELECT
    pm.node_id,
    pm.month,
    pm.failure_rate,
    pm.total_rounds_participated,
    pm.total_successful_rounds,
    ROUND(
      (pm.total_node_metric_count::numeric / mmc.max::numeric) * 100,
      2
    ) as uptime,
    ROUND(wpm.realtime_avg_seconds, 3) as realtime_avg_seconds,
    ROUND(wpm.precomp_avg_seconds, 3) as precomp_avg_seconds
  FROM performance_metrics pm
  JOIN max_metric_counts mmc
  ON mmc.month = pm.month
  JOIN weighted_performance_metrics wpm
  ON pm.node_id = wpm.node_id AND pm.month = wpm.month;

  CREATE UNIQUE INDEX IF NOT EXISTS ${table}_node_id_idx ON ${table} (node_id, month);
`);

exports.down = () => {};
