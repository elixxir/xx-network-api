const fs = require('fs');

const sql = fs.readFileSync('./database/views/weekly/node_average_round_metrics.sql').toString();
const table = 'weekly_node_average_round_metrics';

exports.up = (knex) => knex.raw(`
  CREATE MATERIALIZED VIEW IF NOT EXISTS ${table}
  AS SELECT * FROM dblink(
    '${process.env.REMOTE_DB_URL}',
    '${sql.replace(/'/g, '\'\'')}'
  ) as t(node_id text, realtime_avg_seconds numeric, precomp_avg_seconds numeric, week timestamp)
  WITH NO DATA;

  CREATE UNIQUE INDEX ${table}_node_id_idx ON ${table} (node_id, week);
`);

exports.down = (knex) => knex.raw(`DROP MATERIALIZED VIEW IF EXISTS ${table}`);
