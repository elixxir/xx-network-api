exports.up = (knex) => knex.raw(`
  ALTER MATERIALIZED VIEW node_uptimes RENAME TO monthly_node_uptimes;
  ALTER MATERIALIZED VIEW node_failure_rates RENAME TO monthly_node_failure_rates;
`);

exports.down = (knex) => knex.raw(`
  ALTER MATERIALIZED VIEW monthly_node_uptimes RENAME TO node_uptimes;
  ALTER MATERIALIZED VIEW monthly_node_failure_rates RENAME TO node_failure_rates;
`);
