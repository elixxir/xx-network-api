exports.up = async (knex) => {
  await knex.schema.table('network_performance_metrics', (table) => {
    table.integer('precomp_rounds_failed').nullable().defaultTo(null);
    table.integer('realtime_rounds_failed').nullable().defaultTo(null);
  });
};

exports.down = (knex) => knex.schema.table('network_performance_metrics', (table) => {
  table.dropColumn('precomp_rounds_failed');
  table.dropColumn('realtime_rounds_failed');
});
