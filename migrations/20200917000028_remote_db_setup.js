
exports.up = async (knex) => {
  await knex.raw('create extension dblink;');
};

exports.down = async (knex) => {
  await knex.raw('DROP EXTENSION IF EXISTS dblink;');
};
