const table = 'hourly_network_metrics';

exports.up = (knex) => knex.raw(`
  CREATE MATERIALIZED VIEW IF NOT EXISTS ${table}
  AS WITH network_metrics AS (
    SELECT
      DATE_TRUNC('hour', timezone('GMT', interval_start)) AS hour,
      ROUND(CAST(SUM(rounds_failed) as numeric) / NULLIF(CAST(SUM(rounds_participated) as numeric), 0) * 100, 2)  as failure_rate,
      SUM(rounds_participated) - SUM(rounds_failed) as total_successful_rounds
    FROM node_performance_metrics
    GROUP BY hour
  ), weighted_performance_metrics AS (
    SELECT
      hour,
      SUM(realtime_avg_seconds * weight) AS realtime_avg_seconds,
      SUM(precomp_avg_seconds * weight) AS precomp_avg_seconds
    FROM (
      SELECT
        t.hour,
        successful_rounds / NULLIF(pm.total_successful_rounds::numeric, 0) as weight,
        realtime_avg_seconds,
        precomp_avg_seconds
        FROM (
          SELECT
            date_trunc('hour', timezone('GMT', interval_start)) as hour,
            realtime_avg_seconds,
            precomp_avg_seconds,
            rounds_participated - rounds_failed as successful_rounds
          FROM node_performance_metrics npm
        ) t
        JOIN network_metrics pm
        ON pm.hour = t.hour
      ) z
    GROUP BY hour
  ), network_tps AS (
    SELECT
      batch_size_sum / extract('epoch' from interval_end - interval_start) AS tps,
      DATE_TRUNC('hour', timezone('GMT', interval_start)) AS hour,
      interval_end
    FROM network_performance_metrics
  )
  SELECT
    pm.failure_rate,
    pm.total_successful_rounds,
    ROUND(CAST(nt.tps AS numeric), 2) as network_tps,
    ROUND(wpm.realtime_avg_seconds, 3) as realtime_avg_seconds,
    ROUND(wpm.precomp_avg_seconds, 3) as precomp_avg_seconds,
    pm.hour as interval_start,
    timezone('GMT', nt.interval_end) as interval_end
  FROM network_metrics pm
  JOIN weighted_performance_metrics wpm
  ON pm.hour = wpm.hour
  JOIN network_tps nt
  ON pm.hour = nt.hour;

  CREATE UNIQUE INDEX ${table}_interval_start_idx ON ${table} (interval_start);
  CREATE UNIQUE INDEX ${table}_interval_end_idx ON ${table} (interval_end);
`);

exports.down = (knex) => knex.raw(`DROP MATERIALIZED VIEW IF EXISTS ${table}`);
