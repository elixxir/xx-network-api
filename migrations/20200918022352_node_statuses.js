const fs = require('fs');

const sql = fs.readFileSync('./database/views/node_statuses.sql').toString();

exports.up = (knex) => knex.raw(`
  CREATE MATERIALIZED VIEW IF NOT EXISTS node_statuses
  AS SELECT * FROM dblink(
    '${process.env.REMOTE_DB_URL}',
    '${sql.replace(/'/g, '\'\'')}'
  ) as t(node_id text, status text)
  WITH NO DATA;

  CREATE UNIQUE INDEX node_statuses_node_id_idx ON node_statuses (node_id);
`);

exports.down = (knex) => knex.raw('DROP MATERIALIZED VIEW IF EXISTS node_statuses');
