require('dotenv').config();
require('express-async-errors');
require('./scripts/refresh_views');

const app = require('express')();
const helmet = require('helmet');
const passport = require('passport');
const bodyParser = require('body-parser');
const Sentry = require('@sentry/node');
const Tracing = require('@sentry/tracing');
const compression = require('compression');

const cors = require('cors');
// const rateLimit = require('express-rate-limit');

const { routes, initialize } = require('./routes');

const port = process.env.PORT || 4000;

Sentry.init({
  dsn: 'https://fae159c107b14dd8bacfe6d0dcd3e047@o268001.ingest.sentry.io/5285751',
  environment: process.env.NODE_ENV,
  integrations: [
    new Sentry.Integrations.Http({ tracing: true }),
    new Tracing.Integrations.Express({
      app,
    }),
    new Tracing.Integrations.Postgres(),
  ],
  tracesSampleRate: 1.0,
});

app.use(Sentry.Handlers.requestHandler());
app.use(Sentry.Handlers.tracingHandler());
app.use(compression());
app.use(helmet());
app.use(helmet.hidePoweredBy());
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(passport.initialize());

// const apiLimiter = rateLimit({
//   windowMs: 15 * 60 * 1000, // 15 minutes
//   max: 15 * 60 * 2, // 2 per second
// });

// app.use('/v1', apiLimiter);
app.get('/', (_req, res) => res.redirect('/v1/docs'));
app.get('/v1', (_req, res) => res.redirect('/v1/docs'));
app.use('/v1', routes);

app.use(Sentry.Handlers.errorHandler());

(async () => {
  console.log('Initializing...');
  initialize();

  app.listen(port, () => {
    console.log(`Listening on ${port}`); // eslint-disable-line
  });
})();

module.exports = app;
